from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptFrom, ExpenseCategoryForm, AccountForm

# Create your views here
@login_required
def receipts_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt,
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptFrom(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptFrom()
        form.fields["category"].queryset = ExpenseCategory.objects.filter(owner=request.user)
        form.fields["account"].queryset = Account.objects.filter(owner=request.user)
    context = {
        "form" : form
    }
    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category,
    }
    return render(request, "receipts/expense.html", context)

@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account,
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form" : form,
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form" : form,
    }
    return render(request, "receipts/create_account.html", context)

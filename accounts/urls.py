from django.urls import path
from accounts.views import user_login, user_logout, user_sign_up

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", user_sign_up, name="signup"),
]
